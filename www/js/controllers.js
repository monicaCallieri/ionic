angular.module('app.controllers', [])

        .controller('homeCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
            function ($scope, $stateParams) {


            }])

        .controller('cartCtrl', ['$scope', '$stateParams', '$http', 'Request', 'Post',
            // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
            function ($scope, $stateParams, $http, Request, Post) {
                $scope.postRequest = function () {
                    console.log("funzione postRequest");
                    $http.post("https://jsonplaceholder.typicode.com/users", {data: {"name": 'peter'}})
                            .success(function (data, status, headers, config) {
                                console.log('data success');
                                console.log(data); // for browser console
                                $scope.result = data; // for UI
                            })
                            .error(function (data, status, headers, config) {
                                console.log('data error');
                            })
                            .then(function (result) {
                                things = result.data;
                            });
                };
                $scope.getRequest = function () {
                    console.log("funzione getRequest");
                    $http.get("https://api.myjson.com/bins/18dorn")
                            .success(function (data, status, headers, config) {
                                console.log('data success');
                                console.log(data); // for browser console
                                $scope.result = data; // for UI
                            })
                            .error(function (data, status, headers, config) {
                                console.log('data error');
                            })
                            .then(function (result) {
                                things = result.data;
                            });
                };
                $scope.postRequestResource = function () {
                    console.log("funzione postRequestResource");
                    var utente = {user: "test", password: "password"};
                    Post.save((utente), function (result) {
                        $scope.id = result.id;
                        console.log(result.user);
                        console.log($scope.id);
                    });
                };
                $scope.calciatori;
                $scope.getRequestResource = function () {
                    console.log("funzione getRequestResource");
                    $scope.calciatori = Request.get(function (result) {
                        $scope.calciatori = result.personaggi;
                        console.log($scope.calciatori[0].nome + " " + $scope.calciatori[0].cognome);
                    });
                };
                $scope.loadCalciatore = function (id) {
                    console.log("funzione loadCalciatore");
                    $scope.currentCalciatore = $scope.calciatori[parseInt(id) - 1];
                };
            }])

        .controller('cloudCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
            function ($scope, $stateParams) {

            }])

        .controller('menuCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
            function ($scope, $stateParams) {
                $scope.insert = 'false';
                $scope.usernameTest = "test";
                $scope.username;
                $scope.save = function (username) {
                    console.log("funzione save");
                    $scope.username = username;
                    $scope.insert = 'true';
                    console.log("valore username: " + $scope.username);
                    console.log("valore inserito: " + $scope.insert);
                };
                /*function(){
                 console.log("Sono nella funzione");
                 }*/
                /**/

            }])

        .controller('exampleNewPageCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
            function ($scope, $stateParams) {


            }])

        .controller('mapCtrl', ['$scope', '$stateParams', '$state', '$cordovaGeolocation', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
            function ($scope, $stateParams, $state, $cordovaGeolocation) {
                //initMap();
                var options = {timeout: 10000, enableHighAccuracy: true};
                $cordovaGeolocation.getCurrentPosition(options).then(function (position) {

                    var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    var mapOptions = {
                        center: latLng,
                        zoom: 15,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    console.log("latLng: " + latLng);
                    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
                    /*google.maps.event.addListenerOnce($scope.map, 'idle', function () {

                        var marker = new google.maps.Marker({
                            map: $scope.map,
                            animation: google.maps.Animation.DROP,
                            position: latLng
                        });*/
                        google.maps.event.addListenerOnce($scope.map, 'idle', function () {

                            var marker = new google.maps.Marker({
                                map: $scope.map,
                                animation: google.maps.Animation.DROP,
                                position: latLng
                            });

                            var infoWindow = new google.maps.InfoWindow({
                                content: "Here I am!"
                            });

                            google.maps.event.addListener(marker, 'click', function () {
                                infoWindow.open($scope.map, marker);
                            });

                        //});

                    });
                }, function (error) {
                    console.log("Could not get location");
                });

            }])

        .controller('exampleCtrl', ['$scope', '$stateParams', '$ionicHistory', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
            function ($scope, $stateParams, $ionicHistory, $state) {
                $scope.goBack = function () {
                    //$ionicHistory.goBack();
                    window.history.back();
                };
                $scope.goIndex = function () {
                    $state.go('menu.home');
                };
            }]
                )
 