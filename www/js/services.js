angular.module('app.services', ['ngResource'])

.factory('BlankFactory', [function(){

}])

.factory('Request', ['$resource', function($resource) {
        var resourceUrl =  'https://api.myjson.com/bins/o1iqr';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'save':{
                
            }
        });
}])

.factory('Post', ['$resource', function($resource) {
        var resourceUrl =  'https://jsonplaceholder.typicode.com/users';

        return $resource(resourceUrl, {}, {
            'save':{
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    //copy.registrationDate = DateUtils.convertLocalDateToServer(copy.registrationDate);
                    return angular.toJson(copy);
                }
            }
        });
}])

.service('BlankService', [function(){

}]);